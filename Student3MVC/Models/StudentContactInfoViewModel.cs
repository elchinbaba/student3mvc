﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Student3MVC.Models
{
    public class StudentContactInfoViewModel
    {
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
    }
}
