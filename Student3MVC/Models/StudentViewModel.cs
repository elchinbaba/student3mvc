﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Student3MVC.Models
{
    public class StudentViewModel
    {
        public int Id { get; set; }
        public StudentPersonalInfoViewModel StudentPersonalInfo { get; set; }
        public StudentEducationInfoViewModel StudentEducationInfo { get; set; }
        public StudentContactInfoViewModel StudentContactInfo { get; set; }
    }
}
