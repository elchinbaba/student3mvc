﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Student3MVC.Models
{
    public class StudentEducationInfoViewModel
    {
        public string University { get; set; }
        public string Degree { get; set; }
        public string Faculty { get; set; }
        public string Department { get; set; }
        public string Major { get; set; }
    }
}
