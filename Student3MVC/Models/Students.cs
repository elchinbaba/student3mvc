﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Student3MVC.Models
{
    static public class Students
    {
        static public List<StudentViewModel> All = new()
        {
            new()
            {
                Id = 3,
                StudentPersonalInfo = new()
                {
                    Age = 21,
                    Name = "Elchin",
                    Surname = "Babali"
                },
                StudentEducationInfo = new()
                {
                    University = "ASOIU",
                    Degree = "Bachelor",
                    Faculty = "FITM",
                    Department = "General and Applied Mathematics",
                    Major = "Computer Science"
                },
                StudentContactInfo = new()
                {
                    PhoneNumber = "00994557761921",
                    EmailAddress = "babalielchin@gmail.com"
                }
            },
            new()
            {
                Id = 17,
                StudentPersonalInfo = new()
                {
                    Age = 21,
                    Name = "Nijat",
                    Surname = "Tanriverdiyev"
                },
                StudentEducationInfo = new()
                {
                    University = "ASOIU",
                    Degree = "Bachelor",
                    Faculty = "FITM",
                    Department = "General and Applied Mathematics",
                    Major = "Computer Science"
                },
                StudentContactInfo = new()
                {
                    PhoneNumber = "null",
                    EmailAddress = "nijat.tanriverdiyev@gmail.com"
                }
            }
        };
    }
}
