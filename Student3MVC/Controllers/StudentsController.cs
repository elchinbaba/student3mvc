﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Student3MVC.Models;

namespace Student3MVC.Controllers
{
    public class StudentsController : Controller
    {
        private readonly List<StudentViewModel> _students = new()
        {
            new()
            {
                Id = 3,
                StudentPersonalInfo = new()
                {
                    Age = 21,
                    Name = "Elchin",
                    Surname = "Babali"
                },
                StudentEducationInfo = new()
                {
                    University = "ASOIU",
                    Degree = "Bachelor",
                    Faculty = "FITM",
                    Department = "General and Applied Mathematics",
                    Major = "Computer Science"
                },
                StudentContactInfo = new()
                {
                    PhoneNumber = "00994557761921",
                    EmailAddress = "babalielchin@gmail.com"
                }
            },
            new()
            {
                Id = 17,
                StudentPersonalInfo = new()
                {
                    Age = 21,
                    Name = "Nijat",
                    Surname = "Tanriverdiyev"
                },
                StudentEducationInfo = new()
                {
                    University = "ASOIU",
                    Degree = "Bachelor",
                    Faculty = "FITM",
                    Department = "General and Applied Mathematics",
                    Major = "Computer Science"
                },
                StudentContactInfo = new()
                {
                    PhoneNumber = "null",
                    EmailAddress = "nijat.tanriverdiyev@gmail.com"
                }
            }
        };
        public IActionResult Details(int id)
        {
            return View(Students.All.FirstOrDefault(student => student.Id == id));
        }
        public IActionResult Index()
        {
            return View(Students.All);
        }
    }
}
